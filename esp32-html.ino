/*
ESP32 HTML Multipage with Arduino

How to creat a server with a ESP32 to run HTML pages

Source : https://techtutorialsx.com/2017/12/16/esp32-arduino-async-http-server-serving-a-html-page-from-flash-memory/
*/
#include <Arduino.h>
#include <WiFi.h>
#include <FS.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
 
// Replace the next variables with your SSID/Password combination
const char* ssid = "XXXXXXXX";
const char* password =  "XXXXXXXX";
 
WiFiClient espClient;
AsyncWebServer server(80);

// Constances of ours Webpages
const char header[56] = "<html><title>ESP32 Test HTML</title><head></head><body>";
const char nav[106] = "<h1><a href=\"/\">Accueil</a></h1><ul><li><a href=\"page\">Page</a></li><li><a href=\"test\">Test</a></li></ul>";
const char footer[32] = "<footer></footer></body></html>";




void setup(){
  Serial.begin(9600);
 
  //Wifi Connexion
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");

  //Pages builts
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      //You need to know the sum of all contents lenght add
      char html[233];
      char content[39] = "<h2>Contenu</h2><p>Texte a afficher<p>";
      sprintf(html,"%s %s %s %s",header,nav,content,footer);

    request->send(200, "text/html", html );
  });

  //A page with float beetween texts
  server.on("/page", HTTP_GET, [](AsyncWebServerRequest *request){
      char page[296];
      char content[78] = "<h2>Affichage température</h2><p>Exemple pour afficher un chiffre (float)<p>";
      char contentSuite[15] = "<p>Témprature";
      char temperature_temp[5];
      //Arduino not include float in sprintf() function. It's need to convert with dtostrf()
      float temperature = 21.36;
      dtostrf(temperature, 5, 2, temperature_temp);
      char contentEnd[4] = "<p>";
      sprintf(page,"%s %s %s %s %s %s %s",header,nav,content,contentSuite,temperature_temp,contentEnd,footer);
    request->send(200, "text/html", page);
  });

  //A page with an integral
  server.on("/test", HTTP_GET, [](AsyncWebServerRequest *request){
      char test[208];
      char content[13] = "Un chiffre :";
      //to inclunde int type
      int myInt=5;
      sprintf(test,"%s %s %s = %i %s",header,nav,content,myInt,footer);
      
    request->send(200, "text/html", test);
  });
 
  server.begin();
}
 
void loop(){
}

